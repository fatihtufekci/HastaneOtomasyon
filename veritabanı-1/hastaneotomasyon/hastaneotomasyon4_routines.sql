-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: hastaneotomasyon4
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `doktorbolum`
--

DROP TABLE IF EXISTS `doktorbolum`;
/*!50001 DROP VIEW IF EXISTS `doktorbolum`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `doktorbolum` AS SELECT 
 1 AS `doktorTC`,
 1 AS `ad`,
 1 AS `soyad`,
 1 AS `telefon`,
 1 AS `adres`,
 1 AS `dogumTarihi`,
 1 AS `dogumYeri`,
 1 AS `bolumAdi`,
 1 AS `cinsiyet`,
 1 AS `ucret`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `yatilihastalar`
--

DROP TABLE IF EXISTS `yatilihastalar`;
/*!50001 DROP VIEW IF EXISTS `yatilihastalar`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `yatilihastalar` AS SELECT 
 1 AS `hastaTC`,
 1 AS `ad`,
 1 AS `soyad`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `randevuhastadoktor`
--

DROP TABLE IF EXISTS `randevuhastadoktor`;
/*!50001 DROP VIEW IF EXISTS `randevuhastadoktor`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `randevuhastadoktor` AS SELECT 
 1 AS `randevuID`,
 1 AS `tarih`,
 1 AS `Doktor Adı`,
 1 AS `Doktor Soyadı`,
 1 AS `Hasta Adı`,
 1 AS `Hasta Soyadı`,
 1 AS `ucret`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `randevuluhastalar2`
--

DROP TABLE IF EXISTS `randevuluhastalar2`;
/*!50001 DROP VIEW IF EXISTS `randevuluhastalar2`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `randevuluhastalar2` AS SELECT 
 1 AS `hastaTC`,
 1 AS `ad`,
 1 AS `soyad`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `odahastarandevudoktor`
--

DROP TABLE IF EXISTS `odahastarandevudoktor`;
/*!50001 DROP VIEW IF EXISTS `odahastarandevudoktor`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `odahastarandevudoktor` AS SELECT 
 1 AS `hastaTC`,
 1 AS `ad`,
 1 AS `soyad`,
 1 AS `ucret`,
 1 AS `kacGun`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `randevuhasta`
--

DROP TABLE IF EXISTS `randevuhasta`;
/*!50001 DROP VIEW IF EXISTS `randevuhasta`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `randevuhasta` AS SELECT 
 1 AS `randevuID`,
 1 AS `ad`,
 1 AS `soyad`,
 1 AS `doktorTC`,
 1 AS `tarih`,
 1 AS `saat`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `bolumdoktor`
--

DROP TABLE IF EXISTS `bolumdoktor`;
/*!50001 DROP VIEW IF EXISTS `bolumdoktor`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `bolumdoktor` AS SELECT 
 1 AS `doktorTC`,
 1 AS `ad`,
 1 AS `soyad`,
 1 AS `bolumAdi`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `muayeneucretleri`
--

DROP TABLE IF EXISTS `muayeneucretleri`;
/*!50001 DROP VIEW IF EXISTS `muayeneucretleri`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `muayeneucretleri` AS SELECT 
 1 AS `ad`,
 1 AS `soyad`,
 1 AS `bolumAdi`,
 1 AS `ucret`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `islemler`
--

DROP TABLE IF EXISTS `islemler`;
/*!50001 DROP VIEW IF EXISTS `islemler`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `islemler` AS SELECT 
 1 AS `hastaTC`,
 1 AS `hastaAdi`,
 1 AS `hastaSoyadi`,
 1 AS `bolumAdi`,
 1 AS `doktorAdi`,
 1 AS `doktorSoyadi`,
 1 AS `ucret`,
 1 AS `description`,
 1 AS `tarih`,
 1 AS `saat`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `randevuucreti`
--

DROP TABLE IF EXISTS `randevuucreti`;
/*!50001 DROP VIEW IF EXISTS `randevuucreti`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `randevuucreti` AS SELECT 
 1 AS `ucret`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `randevuluhastalar`
--

DROP TABLE IF EXISTS `randevuluhastalar`;
/*!50001 DROP VIEW IF EXISTS `randevuluhastalar`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `randevuluhastalar` AS SELECT 
 1 AS `hastaTC`,
 1 AS `ad`,
 1 AS `soyad`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `muayenelihastalar`
--

DROP TABLE IF EXISTS `muayenelihastalar`;
/*!50001 DROP VIEW IF EXISTS `muayenelihastalar`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `muayenelihastalar` AS SELECT 
 1 AS `hastaTC`,
 1 AS `ad`,
 1 AS `soyad`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `muayenehasta`
--

DROP TABLE IF EXISTS `muayenehasta`;
/*!50001 DROP VIEW IF EXISTS `muayenehasta`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `muayenehasta` AS SELECT 
 1 AS `hastaTC`,
 1 AS `muayeneID`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `doktorbolum`
--

/*!50001 DROP VIEW IF EXISTS `doktorbolum`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `doktorbolum` AS (select `doktor`.`doktorTC` AS `doktorTC`,`doktor`.`ad` AS `ad`,`doktor`.`soyad` AS `soyad`,`doktor`.`telefon` AS `telefon`,`doktor`.`adres` AS `adres`,`doktor`.`dogumTarihi` AS `dogumTarihi`,`doktor`.`dogumYeri` AS `dogumYeri`,`bolum`.`bolumAdi` AS `bolumAdi`,`doktor`.`cinsiyet` AS `cinsiyet`,`doktor`.`ucret` AS `ucret` from (`doktor` join `bolum` on((`doktor`.`bolumID` = `bolum`.`bolumID`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `yatilihastalar`
--

/*!50001 DROP VIEW IF EXISTS `yatilihastalar`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `yatilihastalar` AS (select `randevu`.`hastaTC` AS `hastaTC`,`hasta`.`ad` AS `ad`,`hasta`.`soyad` AS `soyad` from (`hasta` join `randevu` on((`randevu`.`hastaTC` = `hasta`.`hastaTC`))) where (`hasta`.`yatılı` = 'Evet')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `randevuhastadoktor`
--

/*!50001 DROP VIEW IF EXISTS `randevuhastadoktor`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `randevuhastadoktor` AS (select `randevu`.`randevuID` AS `randevuID`,`randevu`.`tarih` AS `tarih`,`doktor`.`ad` AS `Doktor Adı`,`doktor`.`soyad` AS `Doktor Soyadı`,`hasta`.`ad` AS `Hasta Adı`,`hasta`.`soyad` AS `Hasta Soyadı`,`doktor`.`ucret` AS `ucret` from ((`randevu` join `doktor` on((`doktor`.`doktorTC` = `randevu`.`doktorTC`))) join `hasta` on((`hasta`.`hastaTC` = `randevu`.`hastaTC`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `randevuluhastalar2`
--

/*!50001 DROP VIEW IF EXISTS `randevuluhastalar2`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `randevuluhastalar2` AS (select `randevu`.`hastaTC` AS `hastaTC`,`hasta`.`ad` AS `ad`,`hasta`.`soyad` AS `soyad` from (`randevu` join `hasta` on((`hasta`.`hastaTC` = `randevu`.`hastaTC`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `odahastarandevudoktor`
--

/*!50001 DROP VIEW IF EXISTS `odahastarandevudoktor`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `odahastarandevudoktor` AS (select `oda`.`hastaTC` AS `hastaTC`,`hasta`.`ad` AS `ad`,`hasta`.`soyad` AS `soyad`,`doktor`.`ucret` AS `ucret`,`oda`.`kacGun` AS `kacGun` from (((`oda` join `hasta` on((`hasta`.`hastaTC` = `oda`.`hastaTC`))) join `randevu` on((`randevu`.`hastaTC` = `hasta`.`hastaTC`))) join `doktor` on((`randevu`.`doktorTC` = `doktor`.`doktorTC`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `randevuhasta`
--

/*!50001 DROP VIEW IF EXISTS `randevuhasta`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `randevuhasta` AS (select `randevu`.`randevuID` AS `randevuID`,`hasta`.`ad` AS `ad`,`hasta`.`soyad` AS `soyad`,`randevu`.`doktorTC` AS `doktorTC`,`randevu`.`tarih` AS `tarih`,`saat`.`saat` AS `saat` from ((`randevu` join `hasta` on((`hasta`.`hastaTC` = `randevu`.`hastaTC`))) join `saat` on((`saat`.`saatID` = `randevu`.`saatID`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `bolumdoktor`
--

/*!50001 DROP VIEW IF EXISTS `bolumdoktor`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `bolumdoktor` AS (select `doktor`.`doktorTC` AS `doktorTC`,`doktor`.`ad` AS `ad`,`doktor`.`soyad` AS `soyad`,`bolum`.`bolumAdi` AS `bolumAdi` from (`doktor` join `bolum` on((`bolum`.`bolumID` = `doktor`.`bolumID`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `muayeneucretleri`
--

/*!50001 DROP VIEW IF EXISTS `muayeneucretleri`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `muayeneucretleri` AS (select `doktor`.`ad` AS `ad`,`doktor`.`soyad` AS `soyad`,`bolum`.`bolumAdi` AS `bolumAdi`,`doktor`.`ucret` AS `ucret` from (`doktor` join `bolum` on((`bolum`.`bolumID` = `doktor`.`bolumID`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `islemler`
--

/*!50001 DROP VIEW IF EXISTS `islemler`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `islemler` AS (select `randevu`.`hastaTC` AS `hastaTC`,`hasta`.`ad` AS `hastaAdi`,`hasta`.`soyad` AS `hastaSoyadi`,`bolum`.`bolumAdi` AS `bolumAdi`,`doktor`.`ad` AS `doktorAdi`,`doktor`.`soyad` AS `doktorSoyadi`,`doktor`.`ucret` AS `ucret`,`muayene`.`description` AS `description`,`randevu`.`tarih` AS `tarih`,`saat`.`saat` AS `saat` from (((((`randevu` join `hasta` on((`hasta`.`hastaTC` = `randevu`.`hastaTC`))) join `doktor` on((`doktor`.`doktorTC` = `randevu`.`doktorTC`))) join `bolum` on((`bolum`.`bolumID` = `doktor`.`bolumID`))) join `saat` on((`saat`.`saatID` = `randevu`.`saatID`))) join `muayene` on((`muayene`.`randevuID` = `randevu`.`randevuID`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `randevuucreti`
--

/*!50001 DROP VIEW IF EXISTS `randevuucreti`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `randevuucreti` AS (select `doktor`.`ucret` AS `ucret` from (`randevu` join `doktor` on((`doktor`.`doktorTC` = `randevu`.`doktorTC`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `randevuluhastalar`
--

/*!50001 DROP VIEW IF EXISTS `randevuluhastalar`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `randevuluhastalar` AS (select `randevu`.`hastaTC` AS `hastaTC`,`hasta`.`ad` AS `ad`,`hasta`.`soyad` AS `soyad` from (`randevu` join `hasta` on((`hasta`.`hastaTC` = `randevu`.`hastaTC`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `muayenelihastalar`
--

/*!50001 DROP VIEW IF EXISTS `muayenelihastalar`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `muayenelihastalar` AS (select `randevu`.`hastaTC` AS `hastaTC`,`hasta`.`ad` AS `ad`,`hasta`.`soyad` AS `soyad` from ((`randevu` join `hasta` on((`hasta`.`hastaTC` = `randevu`.`hastaTC`))) join `muayene` on((`muayene`.`randevuID` = `randevu`.`randevuID`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `muayenehasta`
--

/*!50001 DROP VIEW IF EXISTS `muayenehasta`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `muayenehasta` AS (select `randevu`.`hastaTC` AS `hastaTC`,`muayene`.`muayeneID` AS `muayeneID` from (`muayene` join `randevu` on((`randevu`.`randevuID` = `muayene`.`randevuID`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Dumping events for database 'hastaneotomasyon4'
--

--
-- Dumping routines for database 'hastaneotomasyon4'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-10  1:56:54
