-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: hastaneotomasyon4
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gecmisislemler`
--

DROP TABLE IF EXISTS `gecmisislemler`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gecmisislemler` (
  `hastaTC` bigint(11) NOT NULL,
  `hastaAdi` varchar(45) NOT NULL,
  `hastaSoyadi` varchar(45) NOT NULL,
  `bolumAdi` varchar(45) NOT NULL,
  `doktorAdi` varchar(45) NOT NULL,
  `doktorSoyadi` varchar(45) NOT NULL,
  `muayeneUcreti` int(11) NOT NULL,
  `muayeneSonuc` varchar(250) NOT NULL,
  `tarih` date NOT NULL,
  `saat` varchar(6) NOT NULL,
  PRIMARY KEY (`hastaTC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gecmisislemler`
--

LOCK TABLES `gecmisislemler` WRITE;
/*!40000 ALTER TABLE `gecmisislemler` DISABLE KEYS */;
INSERT INTO `gecmisislemler` VALUES (12345543212,'Melek','Şensoy','Acil Servis','Fadime','Öz',50,'dasdasddasd','2017-12-26','09:00'),(12345678985,'Onur','Mavi','Deri Hastalıkları','Faruk','Çöl',70,'Baş ağrısı','2017-12-26','15:30');
/*!40000 ALTER TABLE `gecmisislemler` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-10  1:56:53
