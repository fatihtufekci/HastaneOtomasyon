-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: hastaneotomasyon4
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bolum`
--

DROP TABLE IF EXISTS `bolum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bolum` (
  `bolumID` int(11) NOT NULL AUTO_INCREMENT,
  `bolumAdi` varchar(45) NOT NULL,
  PRIMARY KEY (`bolumID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bolum`
--

LOCK TABLES `bolum` WRITE;
/*!40000 ALTER TABLE `bolum` DISABLE KEYS */;
INSERT INTO `bolum` VALUES (1,'Acil Servis'),(2,'Diş'),(3,'Deri Hastalıkları'),(4,'Beslenme'),(5,'Dermatoloji'),(6,'Estetik'),(7,'Fizik Tedavi'),(8,'Genel Cerrahi'),(9,'Göz'),(10,'KBB'),(11,'Hematoloji'),(12,'Dahiliye'),(13,'Nöroloji'),(14,'Kemoterapi'),(15,'Ortopedi'),(16,'Pedagoji'),(17,'Psikiyatri'),(18,'Radyoloji'),(19,'Üroloji'),(20,'Kadın Doğum'),(21,'Kardiyoloji');
/*!40000 ALTER TABLE `bolum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `bolumdoktor`
--

DROP TABLE IF EXISTS `bolumdoktor`;
/*!50001 DROP VIEW IF EXISTS `bolumdoktor`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `bolumdoktor` AS SELECT 
 1 AS `doktorTC`,
 1 AS `ad`,
 1 AS `soyad`,
 1 AS `bolumAdi`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `doktor`
--

DROP TABLE IF EXISTS `doktor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doktor` (
  `doktorTC` bigint(20) NOT NULL,
  `ad` varchar(20) NOT NULL,
  `soyad` varchar(25) NOT NULL,
  `telefon` char(11) NOT NULL,
  `adres` varchar(40) NOT NULL,
  `dogumTarihi` date NOT NULL,
  `dogumYeri` varchar(40) NOT NULL,
  `bolumID` int(11) NOT NULL,
  `cinsiyet` char(5) NOT NULL,
  `ucret` int(11) NOT NULL,
  PRIMARY KEY (`doktorTC`),
  KEY `bolumID` (`bolumID`),
  CONSTRAINT `doktor_ibfk_1` FOREIGN KEY (`bolumID`) REFERENCES `bolum` (`bolumID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doktor`
--

LOCK TABLES `doktor` WRITE;
/*!40000 ALTER TABLE `doktor` DISABLE KEYS */;
INSERT INTO `doktor` VALUES (12344321566,'Ebru','Kaya','5434332567','İstanbul','1978-06-08','İstanbul',6,'Kadın',35),(12345678912,'Fadime','Öz','05545545454','İstanbul','1986-03-12','Ardahan',1,'Kadın',50),(12345678913,'Mehmet','Yılmaz','05555555555','İstanbul','1983-04-04','Kars',2,'Erkek',60);
/*!40000 ALTER TABLE `doktor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `doktorbolum`
--

DROP TABLE IF EXISTS `doktorbolum`;
/*!50001 DROP VIEW IF EXISTS `doktorbolum`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `doktorbolum` AS SELECT 
 1 AS `doktorTC`,
 1 AS `ad`,
 1 AS `soyad`,
 1 AS `telefon`,
 1 AS `adres`,
 1 AS `dogumTarihi`,
 1 AS `dogumYeri`,
 1 AS `bolumAdi`,
 1 AS `cinsiyet`,
 1 AS `ucret`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `gecmisislemler`
--

DROP TABLE IF EXISTS `gecmisislemler`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gecmisislemler` (
  `hastaTC` bigint(11) NOT NULL,
  `hastaAdi` varchar(45) NOT NULL,
  `hastaSoyadi` varchar(45) NOT NULL,
  `bolumAdi` varchar(45) NOT NULL,
  `doktorAdi` varchar(45) NOT NULL,
  `doktorSoyadi` varchar(45) NOT NULL,
  `muayeneUcreti` int(11) NOT NULL,
  `muayeneSonuc` varchar(250) NOT NULL,
  `tarih` date NOT NULL,
  `saat` varchar(6) NOT NULL,
  PRIMARY KEY (`hastaTC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gecmisislemler`
--

LOCK TABLES `gecmisislemler` WRITE;
/*!40000 ALTER TABLE `gecmisislemler` DISABLE KEYS */;
INSERT INTO `gecmisislemler` VALUES (12345543212,'Melek','Şensoy','Acil Servis','Fadime','Öz',50,'dasdasddasd','2017-12-26','09:00'),(12345678985,'Onur','Mavi','Deri Hastalıkları','Faruk','Çöl',70,'Baş ağrısı','2017-12-26','15:30');
/*!40000 ALTER TABLE `gecmisislemler` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hasta`
--

DROP TABLE IF EXISTS `hasta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hasta` (
  `hastaTC` bigint(11) NOT NULL,
  `ad` varchar(25) NOT NULL,
  `soyad` varchar(45) NOT NULL,
  `telefon` char(11) NOT NULL,
  `adres` varchar(45) NOT NULL,
  `dogumTarihi` date NOT NULL,
  `dogumYeri` varchar(45) NOT NULL,
  `cinsiyet` char(5) NOT NULL,
  `kanGrubu` varchar(5) NOT NULL,
  `sosyalGuvenlik` varchar(8) NOT NULL,
  `yatılı` varchar(5) NOT NULL,
  PRIMARY KEY (`hastaTC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hasta`
--

LOCK TABLES `hasta` WRITE;
/*!40000 ALTER TABLE `hasta` DISABLE KEYS */;
INSERT INTO `hasta` VALUES (12345676543,'Yavuz','Galeri','5435664323','İstanbul','1997-05-13','Tokat','Erkek','A+','Bağkur','Hayır'),(12345678987,'Ferdi','Tayfur','5346547656','İstanbul','1948-12-02','Bursa','Erkek','0+','Emekli','Evet'),(98765432111,'Hasan','Jamileh','5345677645','Suriye','1997-12-01','Şam','Erkek','0+','SSK','Hayır');
/*!40000 ALTER TABLE `hasta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ilac`
--

DROP TABLE IF EXISTS `ilac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ilac` (
  `ilacID` int(11) NOT NULL AUTO_INCREMENT,
  `ilacAdi` varchar(60) NOT NULL,
  PRIMARY KEY (`ilacID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ilac`
--

LOCK TABLES `ilac` WRITE;
/*!40000 ALTER TABLE `ilac` DISABLE KEYS */;
INSERT INTO `ilac` VALUES (1,'parol'),(2,'gayaben'),(3,'nurofen'),(4,'theraflu'),(5,'beloczoc');
/*!40000 ALTER TABLE `ilac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `islemler`
--

DROP TABLE IF EXISTS `islemler`;
/*!50001 DROP VIEW IF EXISTS `islemler`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `islemler` AS SELECT 
 1 AS `hastaTC`,
 1 AS `hastaAdi`,
 1 AS `hastaSoyadi`,
 1 AS `bolumAdi`,
 1 AS `doktorAdi`,
 1 AS `doktorSoyadi`,
 1 AS `ucret`,
 1 AS `description`,
 1 AS `tarih`,
 1 AS `saat`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `muayene`
--

DROP TABLE IF EXISTS `muayene`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `muayene` (
  `muayeneID` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(250) NOT NULL,
  `randevuID` int(11) NOT NULL,
  PRIMARY KEY (`muayeneID`),
  KEY `randevuID` (`randevuID`),
  CONSTRAINT `muayene_ibfk_1` FOREIGN KEY (`randevuID`) REFERENCES `randevu` (`randevuID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `muayene`
--

LOCK TABLES `muayene` WRITE;
/*!40000 ALTER TABLE `muayene` DISABLE KEYS */;
/*!40000 ALTER TABLE `muayene` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `muayenehasta`
--

DROP TABLE IF EXISTS `muayenehasta`;
/*!50001 DROP VIEW IF EXISTS `muayenehasta`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `muayenehasta` AS SELECT 
 1 AS `hastaTC`,
 1 AS `muayeneID`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `muayeneilac`
--

DROP TABLE IF EXISTS `muayeneilac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `muayeneilac` (
  `muayeneilacID` int(11) NOT NULL AUTO_INCREMENT,
  `muayeneID` int(11) NOT NULL,
  `ilacID` int(11) NOT NULL,
  PRIMARY KEY (`muayeneilacID`),
  KEY `muayeneID` (`muayeneID`),
  KEY `ilacID` (`ilacID`),
  CONSTRAINT `muayeneilac_ibfk_1` FOREIGN KEY (`muayeneID`) REFERENCES `muayene` (`muayeneID`),
  CONSTRAINT `muayeneilac_ibfk_2` FOREIGN KEY (`ilacID`) REFERENCES `ilac` (`ilacID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `muayeneilac`
--

LOCK TABLES `muayeneilac` WRITE;
/*!40000 ALTER TABLE `muayeneilac` DISABLE KEYS */;
/*!40000 ALTER TABLE `muayeneilac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `muayenelihastalar`
--

DROP TABLE IF EXISTS `muayenelihastalar`;
/*!50001 DROP VIEW IF EXISTS `muayenelihastalar`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `muayenelihastalar` AS SELECT 
 1 AS `hastaTC`,
 1 AS `ad`,
 1 AS `soyad`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `muayeneucretleri`
--

DROP TABLE IF EXISTS `muayeneucretleri`;
/*!50001 DROP VIEW IF EXISTS `muayeneucretleri`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `muayeneucretleri` AS SELECT 
 1 AS `ad`,
 1 AS `soyad`,
 1 AS `bolumAdi`,
 1 AS `ucret`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `oda`
--

DROP TABLE IF EXISTS `oda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oda` (
  `odaID` int(11) NOT NULL AUTO_INCREMENT,
  `hastaTC` bigint(20) NOT NULL,
  `kacGun` int(11) NOT NULL,
  PRIMARY KEY (`odaID`),
  KEY `hastaTC` (`hastaTC`),
  CONSTRAINT `oda_ibfk_1` FOREIGN KEY (`hastaTC`) REFERENCES `hasta` (`hastaTC`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oda`
--

LOCK TABLES `oda` WRITE;
/*!40000 ALTER TABLE `oda` DISABLE KEYS */;
INSERT INTO `oda` VALUES (8,12345678987,1);
/*!40000 ALTER TABLE `oda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `odahastarandevudoktor`
--

DROP TABLE IF EXISTS `odahastarandevudoktor`;
/*!50001 DROP VIEW IF EXISTS `odahastarandevudoktor`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `odahastarandevudoktor` AS SELECT 
 1 AS `hastaTC`,
 1 AS `ad`,
 1 AS `soyad`,
 1 AS `ucret`,
 1 AS `kacGun`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `personel`
--

DROP TABLE IF EXISTS `personel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personel` (
  `personelTC` bigint(11) NOT NULL,
  `ad` varchar(25) NOT NULL,
  `soyad` varchar(45) NOT NULL,
  `telefon` char(11) NOT NULL,
  `adres` varchar(45) NOT NULL,
  `dogumTarihi` date NOT NULL,
  `dogumYeri` varchar(45) NOT NULL,
  `cinsiyet` char(5) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `unvani` char(8) NOT NULL,
  PRIMARY KEY (`personelTC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personel`
--

LOCK TABLES `personel` WRITE;
/*!40000 ALTER TABLE `personel` DISABLE KEYS */;
INSERT INTO `personel` VALUES (12345678965,'Hüseyin','Kaya','5443443434','Ankara','1996-08-13','Konya','Erkek',NULL,NULL,'personel'),(34567899876,'Süleyman','Lale','5433567641','Ordu','1992-04-14','Nevşehir','Erkek',NULL,NULL,'personel'),(98765432111,'Cüneyt','Tanman','05444444444','İstanbul','1987-07-04','Samsun','Erkek','cuneyt','tanman','personel'),(98765432112,'Feride','Gürsoy','05434333333','İstanbul','1982-05-02','Nevşehir','Kadın','feride','gursoy','yönetici'),(98765432113,'Müslüm','Gürses','05434444444','İstanbul','1976-10-14','Bolu','Erkek','muslum','gurses','personel');
/*!40000 ALTER TABLE `personel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `randevu`
--

DROP TABLE IF EXISTS `randevu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `randevu` (
  `randevuID` int(11) NOT NULL AUTO_INCREMENT,
  `hastaTC` bigint(20) NOT NULL,
  `doktorTC` bigint(20) NOT NULL,
  `tarih` date NOT NULL,
  `saatID` int(11) NOT NULL,
  PRIMARY KEY (`randevuID`),
  KEY `hastaTC` (`hastaTC`),
  KEY `doktorTC` (`doktorTC`),
  KEY `saatID` (`saatID`),
  CONSTRAINT `randevu_ibfk_1` FOREIGN KEY (`hastaTC`) REFERENCES `hasta` (`hastaTC`),
  CONSTRAINT `randevu_ibfk_2` FOREIGN KEY (`doktorTC`) REFERENCES `doktor` (`doktorTC`),
  CONSTRAINT `randevu_ibfk_3` FOREIGN KEY (`saatID`) REFERENCES `saat` (`saatID`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `randevu`
--

LOCK TABLES `randevu` WRITE;
/*!40000 ALTER TABLE `randevu` DISABLE KEYS */;
INSERT INTO `randevu` VALUES (28,12345678987,12345678912,'2017-12-26',2),(30,12345676543,12345678913,'2017-12-27',13),(31,98765432111,12345678913,'2017-12-27',11);
/*!40000 ALTER TABLE `randevu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `randevuhasta`
--

DROP TABLE IF EXISTS `randevuhasta`;
/*!50001 DROP VIEW IF EXISTS `randevuhasta`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `randevuhasta` AS SELECT 
 1 AS `randevuID`,
 1 AS `ad`,
 1 AS `soyad`,
 1 AS `doktorTC`,
 1 AS `tarih`,
 1 AS `saat`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `randevuhastadoktor`
--

DROP TABLE IF EXISTS `randevuhastadoktor`;
/*!50001 DROP VIEW IF EXISTS `randevuhastadoktor`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `randevuhastadoktor` AS SELECT 
 1 AS `randevuID`,
 1 AS `tarih`,
 1 AS `Doktor Adı`,
 1 AS `Doktor Soyadı`,
 1 AS `Hasta Adı`,
 1 AS `Hasta Soyadı`,
 1 AS `ucret`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `randevuluhastalar`
--

DROP TABLE IF EXISTS `randevuluhastalar`;
/*!50001 DROP VIEW IF EXISTS `randevuluhastalar`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `randevuluhastalar` AS SELECT 
 1 AS `hastaTC`,
 1 AS `ad`,
 1 AS `soyad`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `randevuluhastalar2`
--

DROP TABLE IF EXISTS `randevuluhastalar2`;
/*!50001 DROP VIEW IF EXISTS `randevuluhastalar2`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `randevuluhastalar2` AS SELECT 
 1 AS `hastaTC`,
 1 AS `ad`,
 1 AS `soyad`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `randevuucreti`
--

DROP TABLE IF EXISTS `randevuucreti`;
/*!50001 DROP VIEW IF EXISTS `randevuucreti`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `randevuucreti` AS SELECT 
 1 AS `ucret`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `saat`
--

DROP TABLE IF EXISTS `saat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saat` (
  `saatID` int(11) NOT NULL AUTO_INCREMENT,
  `saat` varchar(6) NOT NULL,
  PRIMARY KEY (`saatID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saat`
--

LOCK TABLES `saat` WRITE;
/*!40000 ALTER TABLE `saat` DISABLE KEYS */;
INSERT INTO `saat` VALUES (1,'09:00'),(2,'09:30'),(3,'10:00'),(4,'10:30'),(5,'11:00'),(6,'11:30'),(7,'13:00'),(8,'13:30'),(9,'14:00'),(10,'14:30'),(11,'15:00'),(12,'15:30'),(13,'16:00');
/*!40000 ALTER TABLE `saat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `yatilihastalar`
--

DROP TABLE IF EXISTS `yatilihastalar`;
/*!50001 DROP VIEW IF EXISTS `yatilihastalar`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `yatilihastalar` AS SELECT 
 1 AS `hastaTC`,
 1 AS `ad`,
 1 AS `soyad`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'hastaneotomasyon4'
--

--
-- Dumping routines for database 'hastaneotomasyon4'
--

--
-- Final view structure for view `bolumdoktor`
--

/*!50001 DROP VIEW IF EXISTS `bolumdoktor`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `bolumdoktor` AS (select `doktor`.`doktorTC` AS `doktorTC`,`doktor`.`ad` AS `ad`,`doktor`.`soyad` AS `soyad`,`bolum`.`bolumAdi` AS `bolumAdi` from (`doktor` join `bolum` on((`bolum`.`bolumID` = `doktor`.`bolumID`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `doktorbolum`
--

/*!50001 DROP VIEW IF EXISTS `doktorbolum`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `doktorbolum` AS (select `doktor`.`doktorTC` AS `doktorTC`,`doktor`.`ad` AS `ad`,`doktor`.`soyad` AS `soyad`,`doktor`.`telefon` AS `telefon`,`doktor`.`adres` AS `adres`,`doktor`.`dogumTarihi` AS `dogumTarihi`,`doktor`.`dogumYeri` AS `dogumYeri`,`bolum`.`bolumAdi` AS `bolumAdi`,`doktor`.`cinsiyet` AS `cinsiyet`,`doktor`.`ucret` AS `ucret` from (`doktor` join `bolum` on((`doktor`.`bolumID` = `bolum`.`bolumID`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `islemler`
--

/*!50001 DROP VIEW IF EXISTS `islemler`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `islemler` AS (select `randevu`.`hastaTC` AS `hastaTC`,`hasta`.`ad` AS `hastaAdi`,`hasta`.`soyad` AS `hastaSoyadi`,`bolum`.`bolumAdi` AS `bolumAdi`,`doktor`.`ad` AS `doktorAdi`,`doktor`.`soyad` AS `doktorSoyadi`,`doktor`.`ucret` AS `ucret`,`muayene`.`description` AS `description`,`randevu`.`tarih` AS `tarih`,`saat`.`saat` AS `saat` from (((((`randevu` join `hasta` on((`hasta`.`hastaTC` = `randevu`.`hastaTC`))) join `doktor` on((`doktor`.`doktorTC` = `randevu`.`doktorTC`))) join `bolum` on((`bolum`.`bolumID` = `doktor`.`bolumID`))) join `saat` on((`saat`.`saatID` = `randevu`.`saatID`))) join `muayene` on((`muayene`.`randevuID` = `randevu`.`randevuID`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `muayenehasta`
--

/*!50001 DROP VIEW IF EXISTS `muayenehasta`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `muayenehasta` AS (select `randevu`.`hastaTC` AS `hastaTC`,`muayene`.`muayeneID` AS `muayeneID` from (`muayene` join `randevu` on((`randevu`.`randevuID` = `muayene`.`randevuID`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `muayenelihastalar`
--

/*!50001 DROP VIEW IF EXISTS `muayenelihastalar`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `muayenelihastalar` AS (select `randevu`.`hastaTC` AS `hastaTC`,`hasta`.`ad` AS `ad`,`hasta`.`soyad` AS `soyad` from ((`randevu` join `hasta` on((`hasta`.`hastaTC` = `randevu`.`hastaTC`))) join `muayene` on((`muayene`.`randevuID` = `randevu`.`randevuID`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `muayeneucretleri`
--

/*!50001 DROP VIEW IF EXISTS `muayeneucretleri`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `muayeneucretleri` AS (select `doktor`.`ad` AS `ad`,`doktor`.`soyad` AS `soyad`,`bolum`.`bolumAdi` AS `bolumAdi`,`doktor`.`ucret` AS `ucret` from (`doktor` join `bolum` on((`bolum`.`bolumID` = `doktor`.`bolumID`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `odahastarandevudoktor`
--

/*!50001 DROP VIEW IF EXISTS `odahastarandevudoktor`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `odahastarandevudoktor` AS (select `oda`.`hastaTC` AS `hastaTC`,`hasta`.`ad` AS `ad`,`hasta`.`soyad` AS `soyad`,`doktor`.`ucret` AS `ucret`,`oda`.`kacGun` AS `kacGun` from (((`oda` join `hasta` on((`hasta`.`hastaTC` = `oda`.`hastaTC`))) join `randevu` on((`randevu`.`hastaTC` = `hasta`.`hastaTC`))) join `doktor` on((`randevu`.`doktorTC` = `doktor`.`doktorTC`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `randevuhasta`
--

/*!50001 DROP VIEW IF EXISTS `randevuhasta`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `randevuhasta` AS (select `randevu`.`randevuID` AS `randevuID`,`hasta`.`ad` AS `ad`,`hasta`.`soyad` AS `soyad`,`randevu`.`doktorTC` AS `doktorTC`,`randevu`.`tarih` AS `tarih`,`saat`.`saat` AS `saat` from ((`randevu` join `hasta` on((`hasta`.`hastaTC` = `randevu`.`hastaTC`))) join `saat` on((`saat`.`saatID` = `randevu`.`saatID`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `randevuhastadoktor`
--

/*!50001 DROP VIEW IF EXISTS `randevuhastadoktor`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `randevuhastadoktor` AS (select `randevu`.`randevuID` AS `randevuID`,`randevu`.`tarih` AS `tarih`,`doktor`.`ad` AS `Doktor Adı`,`doktor`.`soyad` AS `Doktor Soyadı`,`hasta`.`ad` AS `Hasta Adı`,`hasta`.`soyad` AS `Hasta Soyadı`,`doktor`.`ucret` AS `ucret` from ((`randevu` join `doktor` on((`doktor`.`doktorTC` = `randevu`.`doktorTC`))) join `hasta` on((`hasta`.`hastaTC` = `randevu`.`hastaTC`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `randevuluhastalar`
--

/*!50001 DROP VIEW IF EXISTS `randevuluhastalar`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `randevuluhastalar` AS (select `randevu`.`hastaTC` AS `hastaTC`,`hasta`.`ad` AS `ad`,`hasta`.`soyad` AS `soyad` from (`randevu` join `hasta` on((`hasta`.`hastaTC` = `randevu`.`hastaTC`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `randevuluhastalar2`
--

/*!50001 DROP VIEW IF EXISTS `randevuluhastalar2`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `randevuluhastalar2` AS (select `randevu`.`hastaTC` AS `hastaTC`,`hasta`.`ad` AS `ad`,`hasta`.`soyad` AS `soyad` from (`randevu` join `hasta` on((`hasta`.`hastaTC` = `randevu`.`hastaTC`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `randevuucreti`
--

/*!50001 DROP VIEW IF EXISTS `randevuucreti`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `randevuucreti` AS (select `doktor`.`ucret` AS `ucret` from (`randevu` join `doktor` on((`doktor`.`doktorTC` = `randevu`.`doktorTC`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `yatilihastalar`
--

/*!50001 DROP VIEW IF EXISTS `yatilihastalar`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `yatilihastalar` AS (select `randevu`.`hastaTC` AS `hastaTC`,`hasta`.`ad` AS `ad`,`hasta`.`soyad` AS `soyad` from (`hasta` join `randevu` on((`randevu`.`hastaTC` = `hasta`.`hastaTC`))) where (`hasta`.`yatılı` = 'Evet')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-10  1:58:52
